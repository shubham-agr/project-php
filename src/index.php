<?php require 'header.php';  ?>
<?php

$formData = $_POST;
if($formData) {
    $query = new \Src\Query();
    $query->setTable('user');
    $user = $query->select('email', '1');
    //$user->execute();
    var_dump($user);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Project Php</title>

    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/b04cafdc3b.js"></script>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body class="bg-body"


<main class="h-100">
    <div class="container d-flex h-100">
        <div class="row w-100">
            <div class="col-md-10 m-auto bg-white">
                <div class="row">
                    <div class="col-md-6 p-5 text-center">
                        <div class="display-inline-block pt-3 pb-3 w-100" style="max-width: 350px">
                            <div class="custom-card-header pb-3 text-left">Sign in to your account</div>
                            <form action="index.php" method="post" class="custom-form">
                                <div class="">
                                    <div class="form-group text-left">
                                        <label for="inputEmail">email</label>
                                        <span class="custom-form-icon">
                                            <i class="fas fa-envelope" aria-hidden="true"></i>
                                        </span>
                                        <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Enter email">
                                    </div>
                                    <div class="form-group text-left">
                                        <label for="inputEmail">password</label>
                                        <span class="custom-form-icon">
                                            <i class="fas fa-lock" aria-hidden="true"></i>
                                        </span>
                                        <input type="password" name="password" class="form-control" id="inputPassword" aria-describedby="passwordHelp" placeholder="Enter password">
                                    </div>
                                </div>
                                <div class="centered">
                                    <span class="forgot-password link link--light font-12 text-muted w-100">
                                        Forgot password ?
                                    </span>
                                </div>
                                <div class="pt-2 pb-2">
                                    <button id="login-button" type="submit"
                                            class="btn btn-md w-100 card-3 p-2 custom-btn-success">
                                        Sign in
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 bg-light p-5">
                        <div class="d-flex h-100 text-center">
                            <div class="m-auto">
                                <div class="logo-text m-auto text-uppercase p-3">Project PHP</div>
                                <div class="m-auto f-600">Web Engineering</div>
                                <div class="font-13 f-600 p-3">Don't have an account ?
                                    <span>
                                    <a href="register.html" class="text-success">
                                        Sign Up
                                    </a>
                                    </span>
                                </div>
                                <div class="font-13 f-400 custom-card-links">
                                    <ul class="list-inline list-unstyled">
                                        <li class="list-inline-item"><a href="#">Home</a></li>
                                        <li class="list-inline-item"><a href="#">Privacy</a></li>
                                        <li class="list-inline-item"><a href="">Terms</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="../node_modules/jquery/dist/jquery.js"></script>
<script src="../node_modules/popper.js/dist/popper.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKMGgFp5OdpSKkQN2X5fd7LSLVPANFjy8"></script>

<!-- Plugins JS -->
<script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="js/custom.js"></script>


</body>
</html>
<?php

namespace Src;

class Query
{
    private $table;
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function insert($column, $attributes)
    {
        return $this->db->exec("INSERT INTO {$this->table} ({$column}) VALUES ({$attributes})");
    }

    public function select($column, $attributes)
    {
        return $this->db->exec("SELECT {$column} FROM {$this->table} WHERE ({$attributes})");
    }

    public function delete($column, $value, $operator, $single = false, $clause = null)
    {
        $query =  "DELETE FROM {$this->table} WHERE ";
        if ($single) {
            return $query . "{$clause}";
        }
        return $this->db->exec($query . "{$column} {$operator} {$value}");
    }

    public function update($columns, $clause)
    {
        return $this->db->exec("UPDATE {$this->table} SET {$columns} WHERE {$clause}");
    }

}
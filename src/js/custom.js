// A $( document ).ready() block.
$( document ).ready(function() {


    // Fixed navigator code
    // $(window).scroll(function () {
    //     //if you hard code, then use console
    //     //.log to determine when you want the
    //     //nav bar to stick.
    //     if ($(window).scrollTop() > 280) {
    //         $('#navbar').addClass('navbar-fixed');
    //         $('main').css('margin-top','48px');
    //     }
    //     if ($(window).scrollTop() < 281) {
    //         $('#navbar').removeClass('navbar-fixed');
    //         $('main').css('margin-top','0px');
    //     }
    // });

    $('.slick-autoplay').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
});